export default {
	primary: '#9b59b6',
	backBlue: '#2c3e50',
	backGrey: '#ecf0f1',
	highlight: '#e67e22',
};
