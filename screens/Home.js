// Librairies
import React, { useState } from 'react';
import {
	StyleSheet,
	Text,
	View,
	Image,
	useWindowDimensions,
	TouchableOpacity,
	SafeAreaView,
	Dimensions,
	TextInput,
	Alert,
	ScrollView,
} from 'react-native';
import Colors from '../constants/Colors';
import * as gameActions from '../store/actions/game';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useForm, Controller } from 'react-hook-form';

// Redux
import { useSelector, useDispatch } from 'react-redux';

export default function Home(props) {
	// Variables
	const minimum = useSelector((state) => state.minimum);
	const maximum = useSelector((state) => state.maximum);
	const gameStarted = useSelector((state) => state.gameStarted);
	const solution = useSelector((state) => state.solution);
	const dispatch = useDispatch();
	console.log(solution);
	// Etape 2: React hook form
	const {
		control,
		handleSubmit,
		formState: { errors },
		reset,
	} = useForm();

	// State
	const { height, width } = useWindowDimensions();
	const [steps, setSteps] = useState(1);
	const [instruction, setInstruction] = useState();

	// Methodes
	const onStartGameHandler = () => {
		dispatch(gameActions.startGame());
	};

	const onPropositionPressHandler = ({ proposition }) => {
		if (isNaN(proposition)) {
			Alert.alert('Attention', 'Il faut renseigner un nombre!');
		} else {
			if (proposition == solution) {
				// Partie gagnée
				Alert.alert(
					'Juste prix trouvé',
					`Vous avez trouvé en ${steps} essais!`
				);
				// Arrêter la partie
				dispatch(gameActions.endGame(steps));
				// Vider l'input
				setProposition();
				// Vider les instructions
				setInstruction();
				// Initialiser les étapes à 1
				setSteps(1);
			} else if (proposition < solution) {
				// C'est plus
				setInstruction("C'est plus!");
				setSteps((prevSteps) => prevSteps + 1);
			} else {
				// C'est moins
				setInstruction("C'est moins!");
				setSteps((prevSteps) => prevSteps + 1);
			}
		}
	};

	if (!gameStarted && steps != 1) {
		setInstruction();
		setSteps(1);
		setProposition();
	}

	let errorStyle;
	if (errors.maximumInput) {
		errorStyle = {
			borderColor: Colors.primary,
			borderWidth: 3,
		};
	}

	return (
		<View style={styles.container}>
			<SafeAreaView style={styles.safeArea}>
				<ScrollView contentContainerStyle={{ flex: 1 }}>
					<View style={styles.littleContainer}>
						<Image
							source={require('../assets/logo.png')}
							style={{
								...styles.logo,
								width: width * 0.6,
								height: width * 0.4,
							}}
						/>
						<Text style={styles.slogan}>
							Retrouvez le juste prix entre{' '}
							<Text style={styles.highlight}>{minimum} </Text>
							et <Text style={styles.highlight}>{maximum}</Text>.
						</Text>
						{gameStarted ? (
							<>
								<View style={styles.instruction}>
									<Text style={styles.instructionSteps}>#{steps} </Text>
									<Text style={styles.instructionText}>
										{instruction ? instruction : 'Quel est le juste prix?'}
									</Text>
								</View>
								<View style={styles.proposition}>
									{/* <TextInput
										style={styles.input}
										keyboardType='numeric'
										value={proposition}
										onChangeText={setProposition}
										onFocus={() => setProposition()}
										onSubmitEditing={onPropositionPressHandler}
									/> */}
									<Controller
										control={control}
										render={({ field: { value, onChange } }) => (
											<TextInput
												style={{ ...styles.input, ...errorStyle }}
												keyboardType='numeric'
												value={value}
												onChangeText={(value) => onChange(value)}
												onSubmitEditing={handleSubmit(
													onPropositionPressHandler
												)}
												onFocus={() => reset({ proposition: '' })}
											/>
										)}
										name='proposition'
										defaultValue={minimum.toString()}
										rules={{
											min: {
												value: 0,
												message:
													'Veuillez entrer une valeur correcte (supérieure ou égale à 0)',
											},
											required: {
												value: true,
												message: 'Veuillez entrer une valeur',
											},
										}}
									/>
									<TouchableOpacity
										style={styles.send}
										activeOpacity={0.8}
										onPress={handleSubmit(onPropositionPressHandler)}>
										<Ionicons
											name='arrow-forward'
											color={Colors.highlight}
											size={30}
										/>
									</TouchableOpacity>
									{errors.minimumInput && (
										<Text style={styles.error}>
											{errors.minimumInput.message}
										</Text>
									)}
								</View>
							</>
						) : (
							<TouchableOpacity
								style={styles.submit}
								activeOpacity={0.8}
								onPress={onStartGameHandler}>
								<Text style={styles.submitText}>Commencer</Text>
							</TouchableOpacity>
						)}
					</View>
				</ScrollView>
			</SafeAreaView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'white',
	},
	littleContainer: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	safeArea: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	logo: {
		marginBottom: 15,
	},
	slogan: {
		fontSize: 16,
		textAlign: 'center',
	},
	highlight: {
		color: Colors.highlight,
		fontWeight: 'bold',
	},
	submit: {
		backgroundColor: Colors.primary,
		padding: 10,
		width: 100,
		borderRadius: 5,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 50,
	},
	submitText: {
		color: 'white',
	},
	instruction: {
		backgroundColor: Colors.backBlue,
		padding: 15,
		minWidth: Dimensions.get('window').width * 0.5,
		borderRadius: 5,
		marginTop: 30,
		alignItems: 'center',
		flexDirection: 'row',
	},
	instructionText: {
		color: 'white',
	},
	instructionSteps: {
		color: 'white',
		fontWeight: 'bold',
	},
	proposition: {
		flexDirection: 'row',
		alignItems: 'center',
		marginTop: 30,
		backgroundColor: Colors.backGrey,
		borderRadius: 5,
		borderColor: Colors.primary,
		borderBottomWidth: 3,
		width: 150,
	},
	proposition: {
		flexDirection: 'row',
		alignItems: 'center',
		marginTop: 30,
		backgroundColor: Colors.backGrey,
		borderRadius: 5,
		borderColor: Colors.primary,
		borderBottomWidth: 3,
		width: 150,
	},
	input: {
		padding: 10,
		width: 105,
	},
	error: {
		color: Colors.primary,
	},
});
