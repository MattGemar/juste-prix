// Librairies
import React, { useState } from 'react';
import {
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
	TextInput,
	Dimensions,
	SafeAreaView,
	Alert,
	Keyboard,
	TouchableWithoutFeedback,
} from 'react-native';
import Colors from '../constants/Colors';
import { useSelector, useDispatch } from 'react-redux';
import * as gameActions from '../store/actions/game';
// Etape 1: React hook form
import { useForm, Controller } from 'react-hook-form';

export default function Parameters(props) {
	// Variables
	const minimum = useSelector((state) => state.minimum);
	const maximum = useSelector((state) => state.maximum);
	const dispatch = useDispatch();
	// Etape 2: React hook form
	const {
		control,
		handleSubmit,
		formState: { errors },
	} = useForm();

	// Fonctions
	const onSubmitValuesHandler = ({ minimumInput, maximumInput }) => {
		if (Number(minimumInput) < Number(maximumInput)) {
			dispatch(
				gameActions.updateVariables(Number(minimumInput), Number(maximumInput))
			);
			Alert.alert('Sauvegarde effectué', 'vous avez modifié les paramètres');
			Keyboard.dismiss();
		} else {
			Alert.alert('Erreur', 'Veuillez vérifier les valeurs');
		}
	};

	const onError = (data) => {
		Alert.alert('Erreur', 'Veuillez vérifier les valeurs');
	};

	let errorStyle;
	if (errors.maximumInput) {
		errorStyle = {
			borderColor: Colors.primary,
			borderWidth: 3,
		};
	}

	return (
		<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
			<View style={styles.container}>
				<SafeAreaView style={styles.safeArea}>
					<Text style={styles.title}>Paramètres</Text>
					<View style={styles.form}>
						<Text style={styles.label}>Prix minimum</Text>
						{/* <TextInput
						style={styles.input}
						placeholder='0'
						keyboardType='numeric'
						value={minimumInput.toString()}
						onChangeText={setMinimumInput}
					/> */}
						{/* // Etape 3: React hook form */}
						<Controller
							control={control}
							render={({ field: { value, onChange } }) => (
								<TextInput
									style={{ ...styles.input, ...errorStyle }}
									placeholder='0'
									keyboardType='numeric'
									value={value}
									onChangeText={(value) => onChange(value)}
								/>
							)}
							name='minimumInput'
							defaultValue={minimum.toString()}
							rules={{
								min: {
									value: 0,
									message:
										'Veuillez entrer une valeur correcte (supérieure ou égale à 0)',
								},
								required: {
									value: true,
									message: 'Veuillez entrer une valeur minimum',
								},
							}}
						/>
						{errors.minimumInput && (
							<Text style={styles.error}>{errors.minimumInput.message}</Text>
						)}
						<Text style={styles.label}>Prix maximum</Text>
						{/* <TextInput
						style={styles.input}
						placeholder='1000'
						keyboardType='numeric'
						value={maximumInput.toString()}
						onChangeText={setMaximumInput}
					/> */}
						<Controller
							control={control}
							render={({ field: { value, onChange } }) => (
								<TextInput
									style={{ ...styles.input, ...errorStyle }}
									placeholder='1000'
									keyboardType='numeric'
									value={value}
									onChangeText={(value) => onChange(value)}
								/>
							)}
							name='maximumInput'
							defaultValue={maximum.toString()}
							rules={{
								min: {
									value: 0,
									message:
										'Veuillez entrer une valeur correcte (supérieure ou égale à 0)',
								},
								required: {
									value: true,
									message: 'Veuillez entrer une valeur maximum',
								},
							}}
						/>
						{errors.maximumInput && (
							<Text style={styles.error}>{errors.maximumInput.message}</Text>
						)}
					</View>
					<TouchableOpacity
						style={styles.submit}
						activeOpacity={0.8}
						onPress={handleSubmit(onSubmitValuesHandler, onError)}>
						<Text style={styles.submitText}>Sauvegarder</Text>
					</TouchableOpacity>
				</SafeAreaView>
			</View>
		</TouchableWithoutFeedback>
	);
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: 'white',
		flex: 1,
	},
	safeArea: {
		flex: 1,
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		color: Colors.primary,
		fontWeight: 'bold',
		fontSize: 30,
	},
	form: {
		backgroundColor: Colors.backBlue,
		padding: 30,
		borderRadius: 5,
		marginTop: 30,
		minWidth: Dimensions.get('window').width * 0.8,
		elevation: 1,
		shadowColor: 'black',
		shadowOffset: { width: 0, height: 0 },
		shadowOpacity: 0.15,
		shadowRadius: 50,
	},
	label: {
		color: 'white',
		marginTop: 10,
	},
	input: {
		paddingVertical: Platform.OS === 'ios' ? 10 : 5,
		backgroundColor: '#ecf0f1',
		borderRadius: 5,
		paddingHorizontal: 5,
		marginTop: 5,
		marginBottom: 10,
	},
	submit: {
		backgroundColor: Colors.primary,
		padding: 15,
		borderRadius: 5,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 15,
	},
	submitText: {
		color: 'white',
	},
	error: {
		color: Colors.primary,
	},
});
