// Librairies
import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	SafeAreaView,
	FlatList,
	Dimensions,
	Platform,
} from 'react-native';
import Colors from '../constants/Colors';

// Redux
import { useSelector, useDispatch } from 'react-redux';

export default function Scores(props) {
	//Variables
	const scores = useSelector((state) => state.scores);

	return (
		<View style={styles.container}>
			<SafeAreaView style={styles.safeArea}>
				<Text style={styles.title}>Historique</Text>
				{!scores[0] ? (
					<Text style={styles.contentText}>
						Commencez par jouer votre première partie pour consulter votre
						historique.
					</Text>
				) : (
					<FlatList
						data={scores}
						keyExtractor={(item) => Math.random().toString()}
						renderItem={(item) => (
							<View style={styles.score}>
								<Text style={styles.steps}>
									Etapes:
									<Text style={styles.inlineSteps}>{item.item.steps}</Text>
								</Text>
								<Text style={styles.player}>
									<Text style={styles.player}>{item.item.player}</Text>
								</Text>
							</View>
						)}
					/>
				)}
			</SafeAreaView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'white',
	},
	safeArea: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		color: Colors.primary,
		fontWeight: 'bold',
		fontSize: 26,
		marginBottom: 30,
		marginTop: Platform.OS === 'android' ? 50 : 0,
	},
	contentText: {
		fontSize: 18,
		paddingHorizontal: 15,
		textAlign: 'center',
	},
	score: {
		width: Dimensions.get('window').width * 0.85,
		backgroundColor: Colors.backGrey,
		padding: 15,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	steps: {
		fontWeight: 'bold',
	},
	inlineSteps: {
		color: Colors.primary,
		fontWeight: 'normal',
	},
	player: {
		color: Colors.highlight,
	},
});
